package com.epul.oeuvres.dao;

import com.epul.oeuvres.domain.OeuvreventeEntity;
import com.epul.oeuvres.meserreurs.MonException;
import com.epul.oeuvres.domain.AdherentEntity;
import javax.persistence.EntityTransaction;
import java.util.List;

public class ServiceAdherent extends EntityService  {

	// Mise à jour des caractéristiques d'un adhérent
	// Le booleen indique s'il s'agit d'un nouvel adhérent, auquel cas on fait
	// une création
	/* Insertion d'un adherent
	 * param Adherent unAdherent
	 * */
	public void insertAdherent(AdherentEntity unAdherent) throws MonException {
		try
		{
			EntityTransaction transac = startTransaction();
			transac.begin();
			entitymanager.persist(unAdherent);
			transac.commit();
			entitymanager.close();
		}
		catch (MonException e)
		{
			throw e;
		}
		catch (RuntimeException e)
		{
			new MonException("Erreur de lecture", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	/* Lister les adherents
	 * */
	public List<AdherentEntity> consulterListeAdherents() throws MonException {
		List<AdherentEntity> mesAdherents = null;
		try
		{
			EntityTransaction transac = startTransaction();
			transac.begin();
			mesAdherents = (List<AdherentEntity>)
					entitymanager.createQuery(
							"SELECT a FROM AdherentEntity a " +
									"ORDER BY a.nomAdherent").getResultList();
			entitymanager.close();
		}
		catch (MonException e)
		{
			throw e;
		}
		catch (RuntimeException e)
		{
			new MonException("Erreur de lecture", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesAdherents;
	}

	/* Consultation d'une adherent par son numéro
	 */
	public AdherentEntity adherentById(int numero) throws MonException {
		List<AdherentEntity> adherents = null;
		AdherentEntity unAdherent= null;
		try {
			EntityTransaction transac = startTransaction();
			transac.begin();
			unAdherent =  entitymanager.getReference(AdherentEntity.class, numero);
			entitymanager.close();
		}
		catch (MonException e)
		{
			throw e;
		}
		catch (RuntimeException e)
		{
			new MonException("Erreur de lecture", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return unAdherent;
	}




}
