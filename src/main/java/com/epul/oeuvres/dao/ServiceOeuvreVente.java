package com.epul.oeuvres.dao;

import com.epul.oeuvres.domain.AdherentEntity;
import com.epul.oeuvres.domain.OeuvreventeEntity;
import com.epul.oeuvres.domain.ProprietaireEntity;
import com.epul.oeuvres.meserreurs.MonException;


import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;


public class ServiceOeuvreVente extends EntityService {

    public OeuvreventeEntity consulterOeuvreVente(int id) throws MonException {
        List<OeuvreventeEntity> mesOeuvres = null;
        OeuvreventeEntity uneoeuvre = null;
        try {
            EntityTransaction transac = startTransaction();
            transac.begin();

            mesOeuvres = (List<OeuvreventeEntity>) entitymanager.
                    createQuery("SELECT O FROM OeuvreventeEntity O WHERE O.idOeuvrevente=" + id).getResultList();
            uneoeuvre = mesOeuvres.get(0);
            entitymanager.close();
        } catch (MonException e) {
            throw e;
        } catch (RuntimeException e) {
            new MonException("Erreur de lecture", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uneoeuvre;
    }


    public List<OeuvreventeEntity> consulterListeOeuvreVente() throws MonException {


        List<OeuvreventeEntity> mesOeuvres = null;
        AdherentEntity adherent = new AdherentEntity();
        try {
            EntityTransaction transac = startTransaction();
            transac.begin();
            mesOeuvres = (List<OeuvreventeEntity>)
                    entitymanager.createQuery(
                            "SELECT O FROM OeuvreventeEntity O " +
                                    "ORDER BY O.titreOeuvrevente").getResultList();
            entitymanager.close();
        } catch (MonException e) {
            throw e;
        } catch (RuntimeException e) {
            new MonException("Erreur de lecture", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mesOeuvres;
    }
}