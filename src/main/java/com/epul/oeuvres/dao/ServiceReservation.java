package com.epul.oeuvres.dao;

import com.epul.oeuvres.domain.OeuvreventeEntity;
import com.epul.oeuvres.domain.ProprietaireEntity;
import com.epul.oeuvres.domain.ReservationEntity;
import com.epul.oeuvres.meserreurs.MonException;

import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

public class ServiceReservation extends EntityService{
    public List<ReservationEntity> listerReservations() throws MonException {
        List<ReservationEntity> mesReservations = null;
        try {
            EntityTransaction transac = startTransaction();
            transac.begin();
            mesReservations = (List<ReservationEntity>)
                    entitymanager.createQuery(
                            "SELECT r FROM ReservationEntity r " +
                                    "ORDER BY r.idOeuvrevente").getResultList();
            entitymanager.close();
        } catch (MonException e) {
            throw e;
        } catch (RuntimeException e) {
            new MonException("Erreur de lecture", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mesReservations;
    }

    public List<Object[]> consulterReservations() throws MonException {
        List<Object[]> rs = new ArrayList<>();
        List<ReservationEntity> mesReservations;
        int index = 0;
        Object[] item = new Object[2];
        try {
            mesReservations = this.listerReservations();
            // On déclare les services
            ServiceOeuvreVente unOeuvreVenteService = new ServiceOeuvreVente();
            ServiceProprietaire unProprietaireService = new ServiceProprietaire();

            while (index < mesReservations.size()) {
                ReservationEntity uneR = mesReservations.get(index);
                // on récupère l'oeuvre en vente
                // et on recherche le propriétaire
                int idOeuvreVente = uneR.getIdOeuvrevente();
                OeuvreventeEntity uneOeuvreVente = unOeuvreVenteService.consulterOeuvreVente(idOeuvreVente);
                // on récupère le propriétaire
                ProprietaireEntity unProprietaire =
                        unProprietaireService.consulterProprietaire(uneOeuvreVente.getIdProprietaire());

                item[0] = (Object) uneR;
                item[1] = (Object) unProprietaire;
                rs.add(item);

                //on récupère la date et on la convertit
                index++;
            }
            return rs;
        } catch (Exception exc) {
            throw new MonException(exc.getMessage(), "système");
        }
    }
}
