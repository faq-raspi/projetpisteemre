package com.epul.oeuvres.dao;

import com.epul.oeuvres.domain.ProprietaireEntity;
import com.epul.oeuvres.meserreurs.MonException;

import javax.persistence.EntityTransaction;
import java.util.List;

public class ServiceProprietaire extends EntityService {
    public ProprietaireEntity consulterProprietaire(int id) throws MonException {
        List<ProprietaireEntity> mesProprietaires = null;
        ProprietaireEntity unProprietaire = null;
        try {
            EntityTransaction transac = startTransaction();
            transac.begin();

            mesProprietaires = (List<ProprietaireEntity>)entitymanager.
                    createQuery("SELECT p FROM ProprietaireEntity p WHERE p.idProprietaire=" + id).getResultList();
            unProprietaire = mesProprietaires.get(0);
            entitymanager.close();
        } catch (MonException e) {
            throw e;
        } catch (RuntimeException e) {
            new MonException("Erreur de lecture", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unProprietaire;
    }
}
